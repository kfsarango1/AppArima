/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author kfsarango1
 */
public class ManejadorData {
    
    
    Calculate calc = new Calculate();

    //Metodos calcular Fac ------------------- Inicia ---------------------------------
    public void Calcular_ZT(ArrayList<Double> listaPre, double matriz[][]) {
        int tamanoLista = listaPre.size();
        int zt = 1;
        int ztAux;

        for (int fila = 0; fila < matriz.length; fila++) {
            ztAux = zt;
            for (int col = 0; col < matriz[fila].length; col++) {
                if (ztAux < tamanoLista) {
                    matriz[fila][col] = listaPre.get(ztAux);
                } else {
                    matriz[fila][col] = 0;
                }

                ztAux++;
            }
            zt++;

        }

    }

    public double EncontrarMedia(ArrayList<Double> lista) {
        double acumulador = 0;
        for (Double pre : lista) {
            acumulador += pre;
        }
        return acumulador / lista.size();

    }

    public double[][] CalcularCovarianza(ArrayList<Double> listaPre, double[][] matriz, double media) {
        double[][] new_matriz = new double[matriz.length][(matriz[0].length) + 1];
        int lastCol = matriz[0].length - 1;
        for (int fila = 0; fila < matriz.length; fila++) {
            for (int col = 0; col < matriz[fila].length; col++) {
                if (matriz[fila][col] != 0.0) {
                    if (col == lastCol) {
                        new_matriz[fila][col] = (listaPre.get(fila) - media) * (matriz[fila][col] - media);
                        new_matriz[fila][col + 1] = Math.pow((listaPre.get(fila) - media), 2);
                    } else {
                        new_matriz[fila][col] = (listaPre.get(fila) - media) * (matriz[fila][col] - media);
                    }
                } else {
                    if (col == lastCol) {
                        new_matriz[fila][col + 1] = Math.pow((listaPre.get(fila) - media), 2);
                    }
                }

            }
        }

        return new_matriz;
    }

    public double[] EncontrarMedia(double matriz[][]) {
        int nroCol = matriz[0].length;
        int nroReg = matriz.length - 1;
        double listMedias[] = new double[nroCol];
        int acumulador = 0;
        for (int i = 0; i < nroCol; i++) {
            for (int j = 0; j < matriz.length; j++) {
                acumulador += matriz[j][i];
            }
            listMedias[i] = acumulador / nroReg;

            //System.out.print("\t|\t"+acumulador);
            acumulador = 0;
            nroReg--;
        }
        return listMedias;
    }

    public double[] EncontrarSumatorias(double matriz[][]) {
        int nroCol = matriz[0].length;
        int nroReg = matriz.length - 1;
        double listaSumtatorias[] = new double[nroCol];
        int acumulador = 0;
        for (int i = 0; i < nroCol; i++) {
            for (int j = 0; j < matriz.length; j++) {
                acumulador += matriz[j][i];
            }
            listaSumtatorias[i] = acumulador;

            //System.out.print("\t|\t"+acumulador);
            acumulador = 0;
        }
        return listaSumtatorias;
    }

    public double CalcularProbabilidad(ArrayList<Double> listaPre, boolean signo) {
        double val;
        double div = 1.0 / listaPre.size();
        //System.out.println("tamaño lista "+ listaPre.size());
        //System.out.println("raiz + " + Math.sqrt(div));
        if (signo) {

            val = 1.96 * ((double) Math.sqrt(div));
            return val;
        } else {
            val = 1.96 * ((double) Math.sqrt(div));
            return val * -1;
        }
    }

    public double[] ValoresCo_varianza(double array[], int nroElemPre) {
        int limit = array.length - 1;
        double listCo_varianzas[] = new double[limit];
        for (int i = 0; i < limit; i++) {

            listCo_varianzas[i] = array[i] / nroElemPre;

        }
        return listCo_varianzas;
    }

    public double calValLastSumatoriaYTFinal(double[] array, int nroElemPre) {
        double lastval = array[array.length - 1];
        return lastval / nroElemPre;
    }

    public ArrayList<Double> ValoresFAC(double array[], double promedioFinYT) {
        ArrayList<Double> listaFac = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {

            listaFac.add(array[i] / promedioFinYT);

        }
        return listaFac;
    }



    //Metodos calcular Fac ------------------- Finaliza ---------------------------------
    
    
    
    
    
    //Metodos calcular Raiz Unitaria ------------------- Inicia ---------------------------------
    public double CalcularMatrizRaizUnitaria(ArrayList<Double> listaPre, double[][] matriz) {

        double restdo, restdoXY, restdox_2, restdoEte2, restdoEteFinal2, delta, B1;
        double acumuladorX = 0;
        double acumuladorY = 0;
        double acumuladorX_Y = 0;
        double acumuladorXele2 = 0;
        double acumuladorEte2 = 0;
        double acumuladorFinalEte2 = 0;
        double promedioX;
        double promedioY;
        for (int i = 0; i < matriz.length; i++) {
            if (i != 0) {
                restdo = listaPre.get(i) - listaPre.get(i - 1);
                matriz[i][0] = restdo;
                acumuladorX += restdo;
                if (i < matriz.length - 1) {
                    matriz[i + 1][1] = restdo;
                    acumuladorY += restdo;

                }

            }
        }

        promedioX = acumuladorX / listaPre.size();
        promedioY = acumuladorY / listaPre.size();

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i][0] != 0) {
                matriz[i][2] = matriz[i][0] - promedioX;
            }
            if (matriz[i][1] != 0) {
                matriz[i][3] = matriz[i][1] - promedioY;

                restdoXY = matriz[i][2] * matriz[i][3];
                matriz[i][4] = restdoXY;

                restdox_2 = Math.pow(matriz[i][2], 2);
                matriz[i][5] = restdox_2;

                matriz[i][6] = Math.pow(matriz[i][3], 2);

                acumuladorX_Y += restdoXY;

                acumuladorXele2 += restdox_2;
            }

        }
        delta = acumuladorX_Y / acumuladorXele2;
        B1 = promedioX * (1 - delta);


        /*Calcular regresion*/
        for (int i = 0; i < matriz.length; i++) {
            //7
            if (matriz[i][1] != 0) {
                matriz[i][7] = (B1 + matriz[i][0]) * delta;

                restdoEte2 = Math.pow(matriz[i][0] - matriz[i][7], 2);
                matriz[i][8] = restdoEte2;
                acumuladorEte2 += restdoEte2;

                matriz[i][9] = matriz[i][0] - matriz[i][7];

                if (i < matriz.length - 1) {
                    matriz[i + 1][10] = matriz[i][9];
                }
                if (matriz[i][10] != 0) {
                    restdoEteFinal2 = Math.pow(matriz[i][9] - matriz[i][10], 2);
                    matriz[i][11] = restdoEteFinal2;
                    acumuladorFinalEte2 += restdoEteFinal2;
                }

            }
        }

//        for (int i = 0; i < matriz.length; i++) {
//            for (int j = 0; j < matriz[i].length; j++) {
//                System.out.print("\t|\t" + String.format("%.2f", matriz[i][j]));
//            }
//            System.out.println("");
//        }
        //System.out.println("et ^ 2 = " + acumuladorEte2 + " etfinal^²= " + acumuladorFinalEte2);
        return delta;

    }

    public double CalcularMatrizRaizUnitaria(ArrayList<Double> listaPre, double[][] matriz, boolean dezplazar) {

        double restdo, restdoXY, restdox_2, restdoEte2, restdoEteFinal2, delta, B1;
        double acumuladorX = 0;
        double acumuladorY = 0;
        double acumuladorX_Y = 0;
        double acumuladorXele2 = 0;
        double acumuladorEte2 = 0;
        double acumuladorFinalEte2 = 0;
        double promedioX;
        double promedioY;
        for (int i = 0; i < matriz.length; i++) {
            if (i > 1) {
                restdo = listaPre.get(i) - (2 * listaPre.get(i - 1)) + listaPre.get(i - 2);
                matriz[i][0] = restdo;
                acumuladorX += restdo;
                if (i < matriz.length - 1) {
                    matriz[i + 1][1] = restdo;
                    acumuladorY += restdo;

                }

            }
        }

        promedioX = acumuladorX / listaPre.size();
        promedioY = acumuladorY / listaPre.size();

        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i][0] != 0) {
                matriz[i][2] = matriz[i][0] - promedioX;
            }
            if (matriz[i][1] != 0) {
                matriz[i][3] = matriz[i][1] - promedioY;

                restdoXY = matriz[i][2] * matriz[i][3];
                matriz[i][4] = restdoXY;

                restdox_2 = Math.pow(matriz[i][2], 2);
                matriz[i][5] = restdox_2;

                matriz[i][6] = Math.pow(matriz[i][3], 2);

                acumuladorX_Y += restdoXY;

                acumuladorXele2 += restdox_2;
            }

        }

        delta = acumuladorX_Y / acumuladorXele2;
        B1 = promedioX * (1 - delta);


        /*Calcular regresion*/
        for (int i = 0; i < matriz.length; i++) {
            //7
            if (matriz[i][1] != 0) {
                matriz[i][7] = (B1 + matriz[i][0]) * delta;

                restdoEte2 = Math.pow(matriz[i][0] - matriz[i][7], 2);
                matriz[i][8] = restdoEte2;
                acumuladorEte2 += restdoEte2;

                matriz[i][9] = matriz[i][0] - matriz[i][7];

                if (i < matriz.length - 1) {
                    matriz[i + 1][10] = matriz[i][9];
                }
                if (matriz[i][10] != 0) {
                    restdoEteFinal2 = Math.pow(matriz[i][9] - matriz[i][10], 2);
                    matriz[i][11] = restdoEteFinal2;
                    acumuladorFinalEte2 += restdoEteFinal2;
                }

            }
        }
        

        return delta;

    }
    //Metodos calcular Raiz Unitaria ------------------- Finaliza ---------------------------------

    //Metodos calcular Arima ------------------- Inicio ---------------------------------
    public double CalcularMatrizArima(ArrayList<Double> listaPre, double[][] matriz) {
        ArrayList<double[]> lstAdicional = new ArrayList<>();
        double[] arrayAux = new double[5];
        double rCuadrado;
        int tamanioLista = listaPre.size();
        int startLista;

        int idxzt = 5;
        int idxzt_1 = 4;
        int idxzt_2 = 3;
        int idxzt_3 = 2;
        int idxzt_4 = 1;

        double acumMa = 0;
        double acumYexp2 = 0;

        double mediaMa;
        double valYexp2;
        double valYgeneral;

        for (int i = 0; i < matriz.length; i++) {
            //calculando at
            if (i > 0) {
                matriz[i][0] = listaPre.get(i) - listaPre.get(i - 1);
            }
            //calculando valores de ztN
            if (i > 4) {
                matriz[i][1] = listaPre.get(idxzt);
                matriz[i][2] = listaPre.get(idxzt_1);
                matriz[i][3] = listaPre.get(idxzt_2);
                matriz[i][4] = listaPre.get(idxzt_3);
                matriz[i][5] = listaPre.get(idxzt_4);

                idxzt++;
                idxzt_1++;
                idxzt_2++;
                idxzt_3++;
                idxzt_4++;
            }

        }

        int idxStart = 1;
        double mediaListaPre = EncontrarMedia(listaPre);

        for (int i = 0; i < matriz.length; i++) {
            if (i > 4) {
                matriz[i][6] = Promedio4AT(matriz, idxStart);
                idxStart++;
                acumMa += matriz[i][6];
            }
        }

        mediaMa = acumMa / tamanioLista;

        for (int i = 0; i < matriz.length; i++) {
            if (i > 4) {
                matriz[i][7] = matriz[i][1] - mediaListaPre;
                matriz[i][8] = matriz[i][2] - mediaListaPre;
                matriz[i][9] = matriz[i][3] - mediaListaPre;
                matriz[i][10] = matriz[i][4] - mediaListaPre;
                matriz[i][11] = matriz[i][5] - mediaListaPre;
                matriz[i][12] = matriz[i][6] - mediaMa;

                matriz[i][13] = matriz[i][7] * matriz[i][8];
                matriz[i][14] = matriz[i][7] * matriz[i][9];
                matriz[i][15] = matriz[i][7] * matriz[i][10];
                matriz[i][16] = matriz[i][7] * matriz[i][11];
                matriz[i][17] = matriz[i][7] * matriz[i][12];

                matriz[i][18] = Math.pow(matriz[i][8], 2);
                matriz[i][19] = Math.pow(matriz[i][9], 2);
                matriz[i][20] = Math.pow(matriz[i][10], 2);
                matriz[i][21] = Math.pow(matriz[i][11], 2);
                matriz[i][22] = Math.pow(matriz[i][12], 2);

                matriz[i][23] = matriz[i][8] * matriz[i][9];
                matriz[i][24] = matriz[i][8] * matriz[i][10];
                matriz[i][25] = matriz[i][8] * matriz[i][11];
                matriz[i][26] = matriz[i][8] * matriz[i][12];

                matriz[i][27] = matriz[i][9] * matriz[i][10];
                matriz[i][28] = matriz[i][9] * matriz[i][11];
                matriz[i][29] = matriz[i][9] * matriz[i][12];

                matriz[i][30] = matriz[i][10] * matriz[i][11];
                matriz[i][31] = matriz[i][10] * matriz[i][12];
                matriz[i][32] = matriz[i][11] * matriz[i][12];

                matriz[i][33] = Math.pow(matriz[i][7], 2);

            }
        }

        double[] sumatoriasCols = ListaSumasColsArima(matriz);
        double[] listaValoresProcesados;
        
        listaValoresProcesados = calc.ProcesarDatosArima(sumatoriasCols, mediaListaPre);
       

        for (int i = 5; i < matriz.length; i++) {
            matriz[i][34] = listaValoresProcesados[0] + listaValoresProcesados[1] * matriz[i][2]
                    + listaValoresProcesados[2] * matriz[i][3] + listaValoresProcesados[3]
                    * matriz[i][4] + listaValoresProcesados[4] * matriz[i][5] + listaValoresProcesados[5]
                    * matriz[i][6];

            matriz[i][35] = matriz[i][34] - mediaListaPre;

            valYexp2 = Math.pow(matriz[i][35], 2);
            matriz[i][36] = valYexp2;
            acumYexp2 += valYexp2;
        }

        valYgeneral = sumatoriasCols[sumatoriasCols.length - 1];
        valYexp2 = acumYexp2;
        rCuadrado = valYexp2 / valYgeneral;

        return rCuadrado;

    }
    
    public double CalcularMatrizArimaRepeat(ArrayList<Double> listaPre, ArrayList<Double> lstAdicional, double[][] matriz) {
        
        lstAdicional = ReconfigurandoListaForArimaRepeat(listaPre);
        double rCuadrado;
        int tamanioLista = lstAdicional.size();
        int startLista;

        int idxzt = 6;
        int idxzt_1 = 5;
        int idxzt_2 = 4;
        int idxzt_3 = 3;
        int idxzt_4 = 2;

        double acumMa = 0;
        double acumYexp2 = 0;

        double mediaMa;
        double valYexp2;
        double valYgeneral;

        for (int i = 0; i < matriz.length; i++) {
            //calculando at
            if (i > 1) {
                matriz[i][0] = lstAdicional.get(i) - lstAdicional.get(i - 1);
            }
            //calculando valores de ztN
            if (i > 5) {
                matriz[i][1] = lstAdicional.get(idxzt);
                matriz[i][2] = lstAdicional.get(idxzt_1);
                matriz[i][3] = lstAdicional.get(idxzt_2);
                matriz[i][4] = lstAdicional.get(idxzt_3);
                matriz[i][5] = lstAdicional.get(idxzt_4);

                idxzt++;
                idxzt_1++;
                idxzt_2++;
                idxzt_3++;
                idxzt_4++;
            }

        }

        int idxStart = 1;
        double mediaListaPre = EncontrarMedia(lstAdicional);

        for (int i = 0; i < matriz.length; i++) {
            if (i > 5) {
                matriz[i][6] = Promedio4AT(matriz, idxStart);
                idxStart++;
                acumMa += matriz[i][6];
            }
        }

        mediaMa = acumMa / tamanioLista;

        for (int i = 0; i < matriz.length; i++) {
            if (i > 5) {
                matriz[i][7] = matriz[i][1] - mediaListaPre;
                matriz[i][8] = matriz[i][2] - mediaListaPre;
                matriz[i][9] = matriz[i][3] - mediaListaPre;
                matriz[i][10] = matriz[i][4] - mediaListaPre;
                matriz[i][11] = matriz[i][5] - mediaListaPre;
                matriz[i][12] = matriz[i][6] - mediaMa;

                matriz[i][13] = matriz[i][7] * matriz[i][8];
                matriz[i][14] = matriz[i][7] * matriz[i][9];
                matriz[i][15] = matriz[i][7] * matriz[i][10];
                matriz[i][16] = matriz[i][7] * matriz[i][11];
                matriz[i][17] = matriz[i][7] * matriz[i][12];

                matriz[i][18] = Math.pow(matriz[i][8], 2);
                matriz[i][19] = Math.pow(matriz[i][9], 2);
                matriz[i][20] = Math.pow(matriz[i][10], 2);
                matriz[i][21] = Math.pow(matriz[i][11], 2);
                matriz[i][22] = Math.pow(matriz[i][12], 2);

                matriz[i][23] = matriz[i][8] * matriz[i][9];
                matriz[i][24] = matriz[i][8] * matriz[i][10];
                matriz[i][25] = matriz[i][8] * matriz[i][11];
                matriz[i][26] = matriz[i][8] * matriz[i][12];

                matriz[i][27] = matriz[i][9] * matriz[i][10];
                matriz[i][28] = matriz[i][9] * matriz[i][11];
                matriz[i][29] = matriz[i][9] * matriz[i][12];

                matriz[i][30] = matriz[i][10] * matriz[i][11];
                matriz[i][31] = matriz[i][10] * matriz[i][12];
                matriz[i][32] = matriz[i][11] * matriz[i][12];

                matriz[i][33] = Math.pow(matriz[i][7], 2);

            }
        }

        double[] sumatoriasCols = ListaSumasColsArima(matriz);
        double[] listaValoresProcesados;
        
        listaValoresProcesados = calc.ProcesarDatosArima(sumatoriasCols, mediaListaPre);

        for (int i = 6; i < matriz.length; i++) {
            matriz[i][34] = listaValoresProcesados[0] + listaValoresProcesados[1] * matriz[i][2]
                    + listaValoresProcesados[2] * matriz[i][3] + listaValoresProcesados[3]
                    * matriz[i][4] + listaValoresProcesados[4] * matriz[i][5] + listaValoresProcesados[5]
                    * matriz[i][6];

            matriz[i][35] = matriz[i][34] - mediaListaPre;

            valYexp2 = Math.pow(matriz[i][35], 2);
            matriz[i][36] = valYexp2;
            acumYexp2 += valYexp2;
        }

        valYgeneral = sumatoriasCols[sumatoriasCols.length - 1];
        valYexp2 = acumYexp2;
        rCuadrado = valYexp2 / valYgeneral;
        
        for (Double val : lstAdicional) {
            System.out.println(val);
        }
        
                for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print("\t|\t" + String.format("%.2f", matriz[i][j]));
            }
            System.out.println("");
        }

        return rCuadrado;

    }
    
    public ArrayList<Double> ReconfigurandoListaForArimaRepeat(ArrayList<Double> lista){
        ArrayList<Double> lstNew = new ArrayList<Double>();
        lstNew.add(0.0);
        for (int i = 1; i < lista.size(); i++) {
            lstNew.add(lista.get(i) - lista.get(i - 1));
        }
        
        return lstNew;
    }

    public double Promedio4AT(double[][] matriz, int idx) {
        int limit = idx + 4;
        double acumulador = 0;
        for (int i = idx; i < limit; i++) {
            acumulador += matriz[i][0];
        }

        return acumulador / 4;

    }

    public double[] ListaSumasColsArima(double[][] matriz) {
        double[] array = new double[matriz[0].length - 3];
        double acumulador;
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                acumulador = 0;
                for (int j = 0; j < matriz.length; j++) {
                    acumulador += matriz[j][i];
                }
                array[i] = acumulador;
            }

        }

        return array;
    }


    public void CalcularPronosticos(double[][] matriz, double[][] matrizPro, ArrayList<Double> listaPre, double[] listaValoresProcesados) {
        int start = matriz.length - 4;
        int tamanioListaPre = listaPre.size();
        int idxzt_1 = tamanioListaPre - 1;
        int idxzt_2 = tamanioListaPre - 2;
        int idxzt_3 = tamanioListaPre - 3;
        int idxzt = tamanioListaPre;
        int fila = 0;
        int col;
        int idxStart = 0;
        double probabilidad;
        for (int i = start; i < matriz.length; i++) {
            col = 0;
            for (int j = 0; j < 7; j++) {
                if (j != 5) {
                    matrizPro[fila][col] = matriz[i][j];
                    col++;

                }

            }
            fila++;

        }
        int iAux;
        for (int i = 0; i < matrizPro.length; i++) {
            if (i > 3) {
                iAux = i - 1;
                probabilidad = listaValoresProcesados[0] + listaValoresProcesados[1] * matrizPro[iAux][1]
                        + listaValoresProcesados[2] * matrizPro[iAux][2] + listaValoresProcesados[3]
                        * matrizPro[iAux][3] + listaValoresProcesados[4] * matrizPro[iAux][4] + listaValoresProcesados[5]
                        * matrizPro[iAux][5];
                listaPre.add(probabilidad);

                matrizPro[i][0] = listaPre.get(tamanioListaPre) - listaPre.get(tamanioListaPre - 1);
                tamanioListaPre++;
                matrizPro[i][1] = listaPre.get(idxzt);
                matrizPro[i][2] = listaPre.get(idxzt_1);
                matrizPro[i][3] = listaPre.get(idxzt_2);
                matrizPro[i][4] = listaPre.get(idxzt_3);

                idxzt++;
                idxzt_1++;
                idxzt_2++;
                idxzt_3++;

                matrizPro[i][5] = Promedio4AT(matrizPro, idxStart);
                idxStart++;
            }

        }
    }

    //Metodos calcular Arima ------------------- Finaliza ---------------------------------
    
    public double[][] CalcularDistPrecipitaciones(double array[], int mes, String url) {
        double[][] matriz = null;
        double precipitacion = array[mes];
        System.out.println("precipitacion: "+precipitacion);
        FileInputStream fsIP;
        try {
            fsIP = new FileInputStream(new File(url)); //Read the spreadsheet that needs to be updated
            HSSFWorkbook wb = new HSSFWorkbook(fsIP); //Access the workbook

            //HSSFSheet worksheet = wb.getSheetAt(0); //Access the worksheet, so that we can update / modify it.
            Sheet firstSheet = wb.getSheetAt(0);
            int cols = firstSheet.getRow(0).getPhysicalNumberOfCells();
            int filas = firstSheet.getLastRowNum() + 4;

            matriz = new double[filas][cols];

            Iterator<Row> iterator = firstSheet.iterator();

            int cl;
            int fl = 0;
            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                Iterator<Cell> cellIterator = nextRow.cellIterator();
                cl = 0;
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue());
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue());
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            matriz[fl][cl] = cell.getNumericCellValue();
                            cl++;
                            break;
                    }
                }
                fl++;
            }
            fsIP.close(); //Close the InputStream
            
            double sumaGeneral = 0.0;
            double acumulador = 0.0;
            int filaChange = matriz.length - 3;
            for (int col = 0; col < matriz[0].length; col++) {
                for (int fila = 0; fila < matriz.length; fila++) {
                    acumulador += matriz[fila][col];
                }
                matriz[filaChange][col] = acumulador;
                sumaGeneral += acumulador;
                acumulador = 0;
            }
            filaChange += 1;
            
            //Calculando porcentajes
            for (int col = 0; col < matriz[0].length; col++) {
                matriz[filaChange][col] =  (matriz[filaChange - 1][col] / sumaGeneral) * 100;
            }
            
            filaChange += 1;
            //Callculando prediccion
            for (int col = 0; col < matriz[0].length; col++) {
                matriz[filaChange][col] =  (matriz[filaChange - 1][col] * precipitacion) / 100;
            }
            

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ManejadorData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ManejadorData.class.getName()).log(Level.SEVERE, null, ex);
        }

        return matriz;
    }
    
    public ArrayList<Double> ValoresPrecipitacion(double [][] matriz){
        ArrayList<Double> lista = new ArrayList<>();
        int lastRow = matriz.length - 1;
        
        for (int col = 0; col < matriz[0].length; col++) {
            lista.add(matriz[lastRow][col]);
        }
        
        return lista;
    }
    
    
    // --------------------------- Calculos Hidro      ----------------------
    public ArrayList<Double> CalcularHidroGeneral(ArrayList<Double> listaPre, double beta, double tiempo, double cn, double lon, double pendiente, double area){
        ArrayList<Double> lista = new ArrayList<>();
        double intensidad;
        double p;
        double pe;
        double tc;
        double tp;
        double tbt;
        double qpu;
        double Qp;
        double num, den;
        for (Double val : listaPre) {
            intensidad = (val * beta * 60) / tiempo;
            p = val * beta;
            num = Math.pow((p / 10.0) - (508.0 / cn) + 5.08, 2);
            den = ((0/10.0) + (2032.0 / cn)) - 20.32;
            pe = ( num / den ) * 10.0;
            tc = (0.000325*(Math.pow(lon, 0.77))) / (Math.pow(pendiente, 0.385)) *  60;
            tp = (tiempo / 2) + (0.6 * tc);
            tbt = (8 * tp) / 3;
            qpu = (0.20833 * area) / (tp / 60);
            Qp = qpu * pe;
            
            lista.add(Qp);
        }
        
        return lista;
        
    }
    
    public int MaximoValEnHidro(ArrayList<Double> lista){
        double maximo = lista.get(0);
        int idx = 0;
        
        for (int i = 0; i < lista.size(); i++) {
            if (maximo < lista.get(i)) {
                maximo = lista.get(i);
                idx = i;
            }
        }
        System.out.println("idx selected: " + idx);
        return idx;
    }
    
    public double [][] MatrizHidroToGrafica(double val, double beta, double tiempo, double cn, double lon, double pendiente, double area){
        double [][] matriz = new double[3][2];
        System.out.println("val de pre mayor: "+ val);
        double intensidad;
        double p;
        double pe;
        double tc;
        double tp;
        double tbt;
        double qpu;
        double Qp;
        double num, den;
            intensidad = (val * beta * 60) / tiempo;
            p = val * beta;
            num = Math.pow((p / 10.0) - (508.0 / cn) + 5.08, 2);
            den = ((0/10.0) + (2032.0 / cn)) - 20.32;
            pe = ( num / den ) * 10.0;
            tc = (0.000325*(Math.pow(lon, 0.77))) / (Math.pow(pendiente, 0.385)) *  60;
            tp = (tiempo / 2) + (0.6 * tc);
            tbt = (8 * tp) / 3;
            qpu = (0.20833 * area) / (tp / 60);
            Qp = qpu * pe;
                        
                 
            matriz[1][0] = tp;
            matriz[1][1] = Qp;
            matriz[2][0] = tbt;
            
           
            
        
        return matriz;
        
    }
    
    
    //----------------------------------------- Valores residuales --------------------------------------------{
    public ArrayList<Double> RecuperarRegresion(double [][] matriz){
        ArrayList<Double> newList = new ArrayList<Double>();
        int tamanioMatriz = matriz.length - 1;
        for (int i = 0; i < matriz.length; i++) {
            if (matriz[i][34] != 0) {
                if (i < tamanioMatriz) {
                    newList.add(matriz[i + 1][34] - matriz[i][34]);
                }
                
            }
            
        }
        return newList;
    }
    
    public void CalcularResiduo_ZT(ArrayList<Double> listaRegre, double matriz[][]) {
        
        int tamanoLista = listaRegre.size();
        int zt = 1;
        int ztAux;

        for (int fila = 0; fila < matriz.length; fila++) {
            ztAux = zt;
            for (int col = 0; col < matriz[fila].length; col++) {
                if (ztAux < tamanoLista) {
                    matriz[fila][col] = listaRegre.get(ztAux);
                } else {
                    matriz[fila][col] = 0;
                }

                ztAux++;
            }
            zt++;

        }

    }
    
}
