/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Logica.Calculate;
import Logica.ManejadorData;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author kfsarangos_96
 */
public class ResiduosJFrame extends javax.swing.JFrame {

    ArrayList<Double> listaRegresion;
    public ArrayList<Double> listaVal_Fac = new ArrayList<>();
    public double[] listaVal_FacP;

    ManejadorData logica = new ManejadorData();
    Presentations presentar = new Presentations();
    Calculate calc = new Calculate();

    /**
     * Creates new form ResiduosJFrame
     */
    public ResiduosJFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        residuosJTBL = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        proPosiLBL = new javax.swing.JLabel();
        probNegLBL = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });

        jLabel1.setText("ANÁLISIS DE RESIDUOS");

        residuosJTBL.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "FAC", "FAC P"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(residuosJTBL);
        if (residuosJTBL.getColumnModel().getColumnCount() > 0) {
            residuosJTBL.getColumnModel().getColumn(0).setResizable(false);
            residuosJTBL.getColumnModel().getColumn(1).setResizable(false);
        }

        jLabel5.setText("Probabilidad:");

        proPosiLBL.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        proPosiLBL.setText("-");

        probNegLBL.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        probNegLBL.setText("-");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/graph.png"))); // NOI18N
        jButton1.setText("Fac");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/graph.png"))); // NOI18N
        jButton2.setText("Fac P");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(probNegLBL)
                            .addComponent(proPosiLBL))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 99, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(107, 107, 107))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(321, 321, 321)
                        .addComponent(jButton1)
                        .addGap(128, 128, 128)
                        .addComponent(jButton2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel5)
                        .addGap(14, 14, 14)
                        .addComponent(proPosiLBL)
                        .addGap(19, 19, 19)
                        .addComponent(probNegLBL)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        DefaultTableModel modelo = (DefaultTableModel) this.residuosJTBL.getModel();

        double[][] matriz_ZT = new double[this.listaRegresion.size()][24];
        double arrayValPronostico;
        double[][] matrizCovarianza;
        double ProbPositivo;
        double ProbNegativo;
        double valR_2;
        double PromedioYtUltimo;
        double[] sumaTotalColumn;
        double[] listaValCo_varianza;

        logica.CalcularResiduo_ZT(listaRegresion, matriz_ZT);

        matrizCovarianza = logica.CalcularCovarianza(listaRegresion, matriz_ZT, logica.EncontrarMedia(listaRegresion));

        ProbPositivo = logica.CalcularProbabilidad(listaRegresion, true);
        ProbNegativo = logica.CalcularProbabilidad(listaRegresion, false);
        this.proPosiLBL.setText(String.format("%.4f", ProbPositivo));
        this.probNegLBL.setText(String.format("%.4f", ProbNegativo));

        sumaTotalColumn = logica.EncontrarSumatorias(matrizCovarianza);
        PromedioYtUltimo = logica.calValLastSumatoriaYTFinal(sumaTotalColumn, listaRegresion.size());
        listaValCo_varianza = logica.ValoresCo_varianza(sumaTotalColumn, listaRegresion.size());
        
        listaVal_Fac = logica.ValoresFAC(listaValCo_varianza, PromedioYtUltimo);

        listaVal_FacP = calc.ProcesarValoresFAC(listaVal_Fac);

        presentar.LoadTable(modelo, listaVal_Fac, listaVal_FacP);
        

    }//GEN-LAST:event_formWindowActivated

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFreeChart graphic;
        DefaultCategoryDataset data = new DefaultCategoryDataset();
        int cont = 1;
        for (int i = 0; i < listaVal_Fac.size(); i++) {
            data.addValue(listaVal_Fac.get(i), "FAC", String.valueOf(cont));
            cont++;
        }

        graphic = ChartFactory.createBarChart("Valores FAC", "Nro Data", "Valor", data,
                PlotOrientation.HORIZONTAL, true, true, false);

        /* Get instance of CategoryPlot */
        CategoryPlot plot = graphic.getCategoryPlot();

        /* Change Bar colors */
        BarRenderer renderer = (BarRenderer) plot.getRenderer();

        renderer.setSeriesPaint(0, Color.ORANGE);

        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0);

        ChartPanel graphicFac = new ChartPanel(graphic);

        JFrame vent = new JFrame("Análisis de Residuos - FAC");

        // And JPanel needs to be added to the JFrame itself!
        vent.getContentPane().add(graphicFac);
        vent.pack();

        vent.setVisible(true);
        vent.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        JFreeChart graphic2;
        DefaultCategoryDataset data2 = new DefaultCategoryDataset();
        int cont = 1;
        for (int i = 0; i < listaVal_FacP.length; i++) {
            data2.addValue(listaVal_FacP[i], "Análisis de Residuos - FAC P", String.valueOf(cont));
            cont++;
        }

        graphic2 = ChartFactory.createBarChart("Valores FAC P", "Nro Data", "Valor", data2,
                PlotOrientation.HORIZONTAL, true, true, false);

        ChartPanel graphicFac = new ChartPanel(graphic2);

        JFrame vent = new JFrame("Valores FAC P");

        // And JPanel needs to be added to the JFrame itself!
        vent.getContentPane().add(graphicFac);
        vent.pack();

        vent.setVisible(true);
        vent.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ResiduosJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ResiduosJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ResiduosJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ResiduosJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ResiduosJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel proPosiLBL;
    private javax.swing.JLabel probNegLBL;
    private javax.swing.JTable residuosJTBL;
    // End of variables declaration//GEN-END:variables
}
